#pragma once
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp> 
#include <list>
#include <vector>

using namespace std;
using namespace cv;

class RadonTransform {
	Mat originalImage;
	Mat cloneImage;
	
	

	float pi = 3.14159265358979323846;
	float k;
	float b;
	float r;
	//float kx, ky;
	//float stepS;
	//float stepTeta;
	map<float, float> kTetaMap;
public:
	Mat kTeta;
	Mat radonImage;
	RadonTransform()
	{
		//stepS = sStep;
		//stepTeta = tetaStep;
		k = 0;
		b = 0;
		//string filename = "C:\\testCat.jpg";
		//originalImage = imread(filename, CV_32FC1);
		originalImage = Mat::zeros(100, 100, CV_32FC1);
		rectangle(originalImage, Rect(30, 30, 40, 40), Scalar(255), -1);

		width = originalImage.rows;
		heigth = originalImage.cols;


		cloneImage = Mat::zeros(Size(width, heigth), CV_32FC1);
		//cloneImage.create(cv::Size(100, 100), CV_32FC1);
		//cloneImage = originalImage.mul(0);
		r = sqrt(pow(width / 2, 2) + pow(heigth / 2, 2));
		radonImage = Mat::zeros(180, r * 2, CV_32FC1);
		radonImage.convertTo(radonImage, COLOR_BGR2GRAY);

		//kTetaMap = map<float, float>();


		kTeta = Mat::zeros(180, r * 2, CV_32FC1);
		kTeta.convertTo(kTeta, COLOR_BGR2GRAY);
	}

	void testFunc()
	{
		float r = sqrt(pow(width / 2, 2) + pow(heigth / 2, 2));

		vector<Point> vector1;
		vector<Point> vector2;
		k = getK(45);
		b = getB(0, 45, k);
		vector1 = equalY(b);
		vector2 = equalX(b);


		drawLine(Point(50, 0), Point(50, 100), Scalar(255, 255, 255));
		imshow("clone", cloneImage);
		radonImage = cloneImage.mul(originalImage);

		imshow("test", radonImage);
		imshow("orig", originalImage);
	}

	int width;
	int heigth;

	void showImage()
	{
		imshow("Original", originalImage);
		imshow("Radon", radonImage);
		imshow("KTeta", kTeta);
	}

	void drawLine(Point pointFirst, Point pointSecond, Scalar color)
	{
		line(cloneImage, pointFirst, pointSecond, color);
	}

	float getB(int s, float teta, float k)
	{
		//teta += pi;
		float b1 = (s * sin(teta * pi / 180) - k * s * cos(teta * pi / 180));
		//float b1 = (s * sin(teta) - k * s * cos(teta));
		return b1;
	}

	float getK(float teta)
	{
		//teta += pi;
		float koef = (-tan(pi / 2 - teta * pi / 180));
		if (teta > 89 && teta < 91)
		{
			koef = 100;
		}
		if (teta < 1 && teta > -1)
		{
			koef = 0;
		}
		return koef;
	}

	bool isEpsSearch(float eps, float y1, float y2)
	{
		if ((y1 - eps) < y2 && y2 < (y1 + eps))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	vector<Point> equalY(float b)//������� ��� ������ ����� � Y=h/2 and y= -h/2
	{
		float borderY1 = heigth / 2;
		float borderY2 = -heigth / 2;

		vector<Point> list;

		float eps = 0.01;

		float x1 = (borderY1 - b) / k;
		float x2 = (borderY2 - b) / k;

		float y1 = 0;
		float y2 = 0;

		if (k > 100 && k < -100)
		{
			y1 = borderY1;
			y2 = borderY2;
		}
		else
		{
			y1 = k * x1 + b;
		}	y2 = k * x2 + b;
		if (isEpsSearch(eps, borderY1, y1))
		{
			if (x1 <= width / 2 && x1 >= -heigth / 2)
				list.push_back(Point(x1 + 50, borderY1 + 50));
		}
		if (isEpsSearch(eps, borderY2, y2))
		{
			if (x2 <= width / 2 && x2 >= -width / 2)
				list.push_back(Point(x2 + 50, borderY2 + 50));
		}

		return list;
	}

	vector<Point> equalX(float b)//������� ��� ������ ����� � Y=h/2 and y= -h/2
	{
		float borderX1 = width / 2;
		float borderX2 = -width / 2;

		vector<Point> list;
		//need init k and b
		float eps = 0.01;

		float y1 = k * borderX1 + b;
		float y2 = k * borderX2 + b;

		float x1 = 0;
		float x2 = 0;
		if (k > -0.009 && k < 0.009)
		{
			x1 = borderX1;
			x2 = borderX2;
		}
		else
		{
			x1 = (y1 - b) / k;
			x2 = (y2 - b) / k;
		}
		if (isEpsSearch(eps, borderX1, x1))
		{
			if (y1 <= heigth / 2 && y1 >= -heigth / 2)
				list.push_back(Point(borderX1 + 50, y1 + 50));
		}
		if (isEpsSearch(eps, borderX2, x2))
		{
			if (y2 <= heigth / 2 && y2 >= -heigth / 2)
				list.push_back(Point(borderX2 + 50, y2 + 50));
		}

		return list;
	}

	Mat Transform(int step_s, int step_teta)
	{
		Mat radonTransformBuffer;
		float r = sqrt(pow(width / 2, 2) + pow(heigth / 2, 2));
		Mat tmp;
		tmp.create(width, heigth, 1);
		float k1 = 0;
		vector<Point> vec_point1;
		vector<Point> vec_point2;

		int counter = 0;
		for (int i = 0; i < 180; i += step_teta) // ��������� 180
		{
			k1 = getK(i);
			
			for (int j = -r; j < r; j += step_s)
			{
				k = k1;
				b = getB(j, i, k);
				vec_point1 = equalX(b);
				vec_point2 = equalY(b);
				counter++;
				if (vec_point1.size() != 0 || vec_point2.size() != 0)
				{

					cloneImage = Mat::zeros(width, heigth, CV_32FC1);
					//cloneImage = originalImage.mul(0);
					radonTransformBuffer = Mat::zeros(width, heigth, CV_32FC1);
					//radonTransformBuffer = originalImage.mul(0);
					if (vec_point1.size() == 2 && vec_point2.size() == 0)
					{
						drawLine(vec_point1[0], vec_point1[1], Scalar(1, 1, 1));
					}
					else if (vec_point1.size() == 0 && vec_point2.size() == 2)
					{
						drawLine(vec_point2[0], vec_point2[1], Scalar(1, 1, 1));
					}
					else if (vec_point1.size() == 1 && vec_point2.size() == 1)
					{
						drawLine(vec_point1[0], vec_point2[0], Scalar(1, 1, 1));
					}

					radonTransformBuffer = cloneImage.mul(originalImage);
					cv::normalize(radonTransformBuffer, radonTransformBuffer, 0, 1, cv::NormTypes::NORM_MINMAX);

					Scalar buf = sum(radonTransformBuffer);

					addPixel(i, j, buf);

					//counter++;
				}
				//else
				//{
				//	addPixel(i, j, Scalar(0, 0, 0, 0));
				//	//counter++;
				//}
			}
			//cv::normalize(radonImage, radonImage, 0, 255, cv::NormTypes::NORM_MINMAX);
		}
		
		return tmp;

	}
		

	void addPixel(int i, int j, Scalar buf)
	{
		int ii = j;
		

	// radonImage.at<double>(i, ii) = double(buf[0] / width);
			
		
		if (j < 0)
		{
			ii = j + r * 2;
			
			radonImage.at<double>(i, ii) = double(buf[0] / width);
		}
		else
		{
			ii = j;
			radonImage.at<double>(i, ii) = double(buf[0] / width);
		}

	}



	Mat toDecart(Mat image)
	{
		int size = 180 * r * 2;
		Mat output = Mat::zeros(image.cols*2, image.cols * 2, CV_32FC1);
		output.convertTo(output, COLOR_BGR2GRAY);

		for (int i = 0; i < image.rows; i++) //teta
		{
			for (int j = 0; j < image.cols; j++) //k
			{

				float kx = j * cos(i * pi / 180);
				float ky = j * sin(i * pi / 180);
				if (kx < 0 && ky >= 0) output.at<double>(kx + image.cols, ky) = image.at<double>(i, j);
				if (kx < 0 && ky < 0) output.at<double>(kx + image.cols, ky + image.cols) = image.at<double>(i, j);
				if (kx >= 0 && ky >= 0) output.at<double>(kx, ky) = image.at<double>(i, j);
				if (kx >= 0 && ky < 0) output.at<double>(kx, ky + image.cols) = image.at<double>(i, j);
			}
		}
		//cv::resize(output, output, Size(output.cols,output.rows),1,1, cv::InterpolationFlags::INTER_CUBIC);
		return output;
	}

	pair<float, float> getKxKy(float k, float teta)
	{
		float kx = k * cos(teta * pi / 180);
		float ky = k * sin(teta * pi / 180);
		pair<float, float> pairK = make_pair(kx, ky);
		return pairK;
	}

	Mat DecordToPolar(Mat image)
	{
		int size = 180 * r * 2;
		Mat polarImage = Mat::zeros(size, size, CV_32FC1);
		polarImage.convertTo(polarImage, COLOR_BGR2GRAY);
		Point2f center = Point2f(size / 2, size / 2);
		double maxRad = sqrt(size * size + size * size) / 2;
		cv::linearPolar(image, polarImage, center, maxRad,cv::WARP_FILL_OUTLIERS);
		return polarImage;
	}

	Mat convertRowsX2(Mat input)
	{
		Mat output = Mat::zeros(input.rows * 2, input.cols, CV_32FC1);
		output.convertTo(output, COLOR_BGR2GRAY);
		input.copyTo(output(Rect(0, 0, input.cols, input.rows)));
		rotate(input, input, 180);
		input.copyTo(output(Rect(0, input.rows, input.cols, input.rows)));
		return output;
	}

};