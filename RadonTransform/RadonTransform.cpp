﻿// RadonTransform.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp> 
#include <list>
#include <vector>
#include "RadonTransform.h"


int main()
{
	int stepS = 1;
	int stepTeta = 1;
	RadonTransform radTrans =  RadonTransform(); 
	//radTrans.toDecart(radTrans.radonImage);
	radTrans.Transform(stepS, stepTeta);
	//Mat bufRad = radTrans.convertRowsX2(radTrans.radonImage);

	//imshow("", bufRad);

	//radTrans.showImage();
	//transpose(radTrans.radonImage, radTrans.radonImage);
	
	cv::dft(radTrans.radonImage, radTrans.radonImage, cv::DftFlags::DFT_ROWS);//, (cv::DftFlags::DFT_ROWS | cv::DftFlags::DFT_SCALE | cv::DftFlags::DFT_COMPLEX_OUTPUT));
	//Mat decordMat = radTrans.toDecart(bufRad);
	//imshow("decard", decordMat);
	imshow("fourie", radTrans.radonImage );
	Mat left = radTrans.radonImage(Rect(0, 0, radTrans.radonImage.cols / 2, radTrans.radonImage.rows));
	Mat right = radTrans.radonImage(Rect(radTrans.radonImage.cols / 2, 0, radTrans.radonImage.cols / 2, radTrans.radonImage.rows));

	imshow("left", left);
	imshow("rigth", right);
	
	Mat help_mat = left.clone();
	right.copyTo(left);
	help_mat.copyTo(right);
	cv::hconcat(left, right, radTrans.radonImage);
	left.release(); right.release(); help_mat.release();

	imshow("hconcat", radTrans.radonImage);

	left = radTrans.radonImage(Rect(0, 0, (radTrans.radonImage.cols / 2), radTrans.radonImage.rows));
	right = radTrans.radonImage(Rect(radTrans.radonImage.cols / 2, 0, radTrans.radonImage.cols / 2, radTrans.radonImage.rows));
	cv::flip(left, left, 1);
	imshow("flip", left);
	left = left(Rect(0, 0, left.cols - 1, left.rows));
	Mat dop_mat = right(Rect(0, 0, 1, right.rows));
	cv::hconcat(dop_mat, left, left);
	imshow("hc", left);
	cv::vconcat(left, right, radTrans.radonImage);
	imshow("vcon", radTrans.radonImage);
	
	Point2f center((float)radTrans.radonImage.cols / 2, (float)radTrans.radonImage.rows / 2);
	double maxRadius = 0.7 * min(radTrans.radonImage.rows, radTrans.radonImage.cols);
	int flags = INTER_LINEAR + WARP_INVERSE_MAP + WARP_FILL_OUTLIERS;
	Mat output = Mat::zeros(radTrans.radonImage.rows * 2, radTrans.radonImage.cols, CV_32FC1);
	cv::linearPolar(radTrans.radonImage, output, center, maxRadius, flags);
	imshow("linPolar", output);
	//////////////////////////////////////////////////
	//////////////////////////////////////////////////
	//cv::dft(radTrans.radonImage, radTrans.radonImage, cv::DFT_INVERSE);
	//imshow("recieved", radTrans.radonImage);
	waitKey(0);

	std::cout << "Hello World!\n";
}
//Михаил Торнадо Мельница Мегафон, [10.04.20 17:27]
////////////////////////////////////////////////////
//  //////////////////////////////////////////////////
//	cv::dft(buffer, buffer, cv::DFT_ROWS | cv::DFT_SCALE | cv::DFT_COMPLEX_OUTPUT);
////////////////////////////////////////////////////
////////////////////////////////////////////////////
//Mat left = buffer(Rect(0, 0, buffer.cols / 2, buffer.rows));
//Mat right = buffer(Rect(buffer.cols / 2, 0, buffer.cols / 2, buffer.rows));
//Mat help_mat = left.clone();
//right.copyTo(left);
//help_mat.copyTo(right);
//cv::hconcat(left, right, buffer);
//left.release(); right.release(); help_mat.release();
////////////////////////////////////////////////////
////////////////////////////////////////////////////
//left = buffer(Rect(0, 0, (buffer.cols / 2), buffer.rows));
//right = buffer(Rect(buffer.cols / 2, 0, buffer.cols / 2, buffer.rows));
//cv::flip(left, left, 1);
//left = left(Rect(0, 0, left.cols - 1, left.rows));
//Mat dop_mat = right(Rect(0, 0, 1, right.rows));
//cv::hconcat(dop_mat, left, left);
//cv::vconcat(left, right, buffer);
////////////////////////////////////////////////////
////////////////////////////////////////////////////
//Point2f center((float)buffer.cols / 2, (float)buffer.rows / 2);
//double maxRadius = 0.7 * min(buffer.rows, buffer.cols);
//int flags = INTER_LINEAR + WARP_INVERSE_MAP + WARP_FILL_OUTLIERS;
//cv::linearPolar(buffer, buffer, center, maxRadius, flags);
//////////////////////////////////////////////////
//////////////////////////////////////////////////
//cv::dft(buffer, buffer, cv::DFT_INVERSE | cv::DFT_COMPLEX_OUTPUT);
